void main() {
  /// TODO : Observer l'erreur remontée 
  
  final int myFinal = 20;
  const int myConst = 40;
  
  myFinal = 3; 
  myConst = 6;

  print(myFinal);
  print(myConst);
  
}

void main2() {
  /// TODO : Observer et corriger l'erreur remontée
  int a = 340;

  const int myConst = DateTime.now().millisecond;
  const int myConst2 = 120 + a;
  
  a = 49;
  assert(a == 49);
  assert(myConst2 == 460);

  print(a);
  print(myConst);
  print(myConst2);
  
}