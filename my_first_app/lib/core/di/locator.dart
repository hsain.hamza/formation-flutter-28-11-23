
import 'package:get_it/get_it.dart';
import 'package:my_first_app/app/modules/login/data/local/providers/login_local_provider.dart';
import 'package:my_first_app/app/modules/login/data/remote/providers/login_remote_provider.dart';
import 'package:my_first_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_first_app/app/modules/products/data/local/providers/products_local_provider.dart';
import 'package:my_first_app/app/modules/products/data/remote/providers/products_remote_provider.dart';
import 'package:my_first_app/app/modules/products/data/repository/products_repository.dart';
import 'package:my_first_app/app/modules/promotion/data/local/providers/promotion_local_provider.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/providers/promotion_remote_provider.dart';
import 'package:my_first_app/app/modules/promotion/data/repository/promotion_repository.dart';

final locator = GetIt.instance;

void setupLocator(){
  locator.registerLazySingleton<PromotionRemoteProvider>(() => PromotionRemoteProviderImpl());
  locator.registerLazySingleton<PromotionRepository>(() => PromotionRepositoryImpl());

  locator.registerLazySingleton<ProductsRemoteProvider>(() => ProductsRemoteProviderImpl());
  locator.registerLazySingleton<ProductsRepository>(() => ProductsRepositoryImpl());

  locator.registerLazySingleton<PromotionLocalProvider>(() => PromotionDbProvider());
  locator.registerLazySingleton<ProductsLocalProvider>(() => ProductsDbProvider());

  locator.registerLazySingleton<LoginRemoteProvider>(() => LoginRemoteProviderImpl());
  locator.registerLazySingleton<LoginLocalProvider>(() => LoginDbProvider());

  locator.registerLazySingleton<LoginRepository>(() => LoginRepositoryImpl());



}
