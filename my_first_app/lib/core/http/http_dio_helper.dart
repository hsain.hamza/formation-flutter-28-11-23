
import 'package:dio/dio.dart';

class HttpDioHelper {
  HttpDioHelper._();

  Dio _client = Dio();

  /// [Dio] client by default
  static final HttpDioHelper _instance = HttpDioHelper._();

  /// Get the [Dio] client http
  static Dio get client => _instance._client;

  /// Set the [Dio] client and provides it for all the application or test
  static set client(Dio dio) {
    _instance._client = dio;
  }

}
