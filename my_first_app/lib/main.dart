import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:my_first_app/app.dart';
import 'package:my_first_app/core/di/locator.dart';
import 'package:my_first_app/core/http/http_dio_helper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

Future<void> _initDatabase() async{
  const dbFileName = "my-app.db";
  final appDocDir = await getApplicationDocumentsDirectory();
  final pathToDb = join(appDocDir.path, dbFileName);
  final database = await databaseFactoryIo.openDatabase(pathToDb);
  locator.registerSingleton<Database>(database);
}
void _initHttpClient(){
  HttpDioHelper.client = Dio(
      BaseOptions(
        baseUrl: "https://demo0635484.mockable.io",
        connectTimeout: const Duration(seconds: 10),
        responseType: ResponseType.json,
      )
  );
}
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  setupLocator();

  await _initDatabase();

  _initHttpClient();

  runApp(const MyApp());
}


