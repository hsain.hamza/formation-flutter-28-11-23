import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/login/bloc/login_bloc.dart';
import 'package:my_first_app/app/modules/products/bloc/products_bloc.dart';
import 'package:my_first_app/app/modules/promotion/bloc/promotion_bloc.dart';
import 'package:my_first_app/app/screens/home/home_page.dart';
import 'package:my_first_app/app/screens/list/list_screen.dart';
import 'package:my_first_app/app/screens/login/login_screen.dart';
import 'package:my_first_app/app/screens/splash/splash_screen.dart';
import 'package:my_first_app/app/screens/utils/routes.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (context) => LoginBloc(),
      child: MaterialApp(
        title: 'Flutter Demo',
        routes: {
          kHomeRoute: (context) =>
              MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (_) => ProductsBloc(),
                  ),

                  BlocProvider(
                    create: (_) => PromotionBloc(),
                  ),
                ],
                child: const HomePage(),
              ),
          kSplashRoute: (context) => const SplashScreen(),
          kListRoute: (context) => const ListScreen(),
          kLoginRoute: (context) => LoginScreen(),
        },
        initialRoute: kSplashRoute,
      ),
    );
  }
}
