import 'package:flutter/material.dart';
class HomeHeader extends StatelessWidget {
  const HomeHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10,),
            child: Text(
              "Find your favorites plants",
              style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 20),
          width: 50,
          height: 60,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black12, width: 4),
            borderRadius: const BorderRadius.all(
              Radius.circular(60),
            ),
          ),
          child: const Center(
            child: Icon(Icons.search_sharp),
          ),
        ),
      ],
    );
  }
}
