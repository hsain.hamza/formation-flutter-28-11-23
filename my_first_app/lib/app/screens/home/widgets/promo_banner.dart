import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/promotion/bloc/promotion_bloc.dart';

class PromoBanner extends StatelessWidget {
  const PromoBanner({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: 140,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color.fromARGB(255, 209, 235, 192),
        ),
        child: BlocBuilder<PromotionBloc, PromotionState>(
          builder: (context, state) {
            print(state);
            if(state is PromotionLoading){
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is PromotionError) {
              return const Center(child:  Icon(Icons.warning_amber));
            }

            if (state is PromotionLoaded) {
              return Padding(
                padding: const EdgeInsets.all(20),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.promotionResponse.promoMessage ?? 'N/A',
                            style: const TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            state.promotionResponse.promoSubtitle ?? 'N/A',
                            style: const TextStyle(
                              fontSize: 16,
                              color: Color.fromARGB(255, 108, 120, 102),
                            ),
                          )
                        ],
                      ),
                    ),
                    CachedNetworkImage(
                      imageUrl: state.promotionResponse.promoImage ?? '',
                      placeholder: (context, url) => const CircularProgressIndicator(),
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                    ),
                  ],
                ),
              );
            }


            return const SizedBox.shrink();

          },
        ),
      ),
    );
  }
}
