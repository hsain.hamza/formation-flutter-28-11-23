import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/products/bloc/products_bloc.dart';
import 'package:my_first_app/app/screens/home/widgets/product_item.dart';

class ProductsSection extends StatelessWidget {
  const ProductsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 250,
      child: BlocBuilder<ProductsBloc, ProductsState>(

        builder: (context, state) {
          if (state is ProductsError) {
            return const Center(
              child: Icon(Icons.error_outline),
            );
          }
          if (state is ProductsLoaded) {
            return ListView.builder(
              itemCount: state.products.length ?? 0,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                final element = state.products[index];
                return ProductItem(
                  title: element.title ?? "N/A",
                  price: element.price ?? "N/A",
                  imgUri: element.picture ?? "",
                );
              },

            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
