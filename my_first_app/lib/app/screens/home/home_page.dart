import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/login/bloc/login_bloc.dart';
import 'package:my_first_app/app/modules/products/bloc/products_bloc.dart';
import 'package:my_first_app/app/modules/promotion/bloc/promotion_bloc.dart';
import 'package:my_first_app/app/screens/home/widgets/home_header.dart';
import 'package:my_first_app/app/screens/home/widgets/products_section.dart';
import 'package:my_first_app/app/screens/home/widgets/promo_banner.dart';
import 'package:my_first_app/app/screens/utils/routes.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AfterLayoutMixin<HomePage> {
  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    BlocProvider.of<PromotionBloc>(context).add(FetchPromotion());
    BlocProvider.of<ProductsBloc>(context).add(FetchProducts());
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)?.settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Home page"),
        actions: [
          IconButton(
            onPressed: () {
              BlocProvider.of<LoginBloc>(context).add(LogoutUser());
              Navigator.pushNamedAndRemoveUntil(context, kLoginRoute, (route) => false);
            },
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<PromotionBloc>(context).add(FetchPromotion());
              BlocProvider.of<ProductsBloc>(context).add(FetchProducts());
            },
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const HomeHeader(),
                  PromoBanner(),

                  BlocConsumer<PromotionBloc, PromotionState>(
                    buildWhen: (prev, state) {
                      return true;
                    },
                    listenWhen: (prev, state) {
                      return true;
                    },
                    builder: (_, state) {
                      return Text("$state");
                    },
                    listener: (_, state) {
                      if (state is PromotionLoaded) {
                        const snackBar = SnackBar(
                          content: Text('Promotion chargée !! '),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                  ),

                  // TODO
                  Container(
                    width: double.infinity,
                    height: 60,
                    color: Colors.red,
                    child: Text(
                      "ICI MON ARGUMENT $arguments",
                      style: const TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  ),
                  BlocBuilder<ProductsBloc, ProductsState>(
                    builder: (_, state) {
                      int productsCount = 0;
                      if (state is ProductsLoaded) {
                        productsCount = state.products.length;
                      }
                      return Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          "Les produits à la une ($productsCount)",
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      );
                    },
                  ),
                  // TODO
                  const ProductsSection(),
                  const Padding(
                    padding: EdgeInsets.all(14.0),
                    child: Text(
                      "Autres produits",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const ProductsSection(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
