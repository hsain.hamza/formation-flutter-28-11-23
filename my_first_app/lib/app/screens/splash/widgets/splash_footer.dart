import 'package:flutter/material.dart';
import 'package:my_first_app/app/screens/home/home_page.dart';
import 'package:my_first_app/app/screens/utils/routes.dart';

class SplashFooter extends StatelessWidget {
  const SplashFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 180,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
           Text(
            "Worldwide delivery\nWithin 10 - 15 day",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
           SizedBox(
            height: 15,
          ),

        ],
      ),
    );
  }

  _navigateToHome(BuildContext context) {
    // Navigator.of(context).push(MaterialPageRoute(builder: (_) => const HomePage()));
    /* Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (_) => const HomePage()),
        (settings)=> false,
    ); */
    // Navigator.pushNamedAndRemoveUntil(context, kHomeRoute, (s) => false);
    Navigator.pushNamed(context, kHomeRoute, arguments: "123");
  }
}
