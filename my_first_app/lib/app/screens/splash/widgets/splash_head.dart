import 'dart:io';

import 'package:flutter/material.dart';

class SplashHeader extends StatelessWidget {
  const SplashHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      width: double.infinity,
      height: 150,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RotatedBox(
            quarterTurns: 3,
            child: Text(
              "Planto.shop",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          VerticalDivider(
            width: 30,
            thickness: 1,
            color: Colors.grey,
          ),
          Text(
            "Plant a\ntree for \nlife",
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
