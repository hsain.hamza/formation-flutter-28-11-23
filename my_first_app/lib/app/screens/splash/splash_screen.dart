import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/login/bloc/login_bloc.dart';
import 'package:my_first_app/app/screens/splash/widgets/splash_body.dart';
import 'package:my_first_app/app/screens/splash/widgets/splash_footer.dart';
import 'package:my_first_app/app/screens/splash/widgets/splash_head.dart';
import 'package:my_first_app/app/screens/utils/routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with AfterLayoutMixin<SplashScreen>{

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    BlocProvider.of<LoginBloc>(context).add(CheckAuth());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (_, state){
        if(state is Authenticated){
          Future.delayed(const Duration(seconds: 2)).then((value){
            Navigator.pushNamedAndRemoveUntil(context, kHomeRoute, (route) => false);
          });
        } else if(state is Unauthenticated){
          Future.delayed(const Duration(seconds: 2)).then((value){
            Navigator.pushNamedAndRemoveUntil(context, kLoginRoute, (route) => false);
          });
        }
      },
      child: const Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              SplashHeader(),
              SplashBody(),
              SplashFooter(),
            ],
          ),
        ),
      ),
    );
  }


}
