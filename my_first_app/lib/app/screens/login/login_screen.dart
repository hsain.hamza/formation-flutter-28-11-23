import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_first_app/app/modules/login/bloc/login_bloc.dart';
import 'package:my_first_app/app/modules/login/data/remote/providers/login_remote_provider.dart';
import 'package:my_first_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_first_app/app/screens/utils/routes.dart';
import 'package:my_first_app/core/di/locator.dart';

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  final LoginRepository _loginRepository = locator.get();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Image.asset("assets/images/login/user.png"),
                  TextFormField(
                    controller: _emailController,
                    decoration: const InputDecoration(
                      labelText: "Identifiant",
                      hintText: "Votre adresse e-mail",
                    ),
                    validator: (val) => val?.isValidEmail() == false
                        ? "Format incorrect"
                        : null,
                  ),
                  TextFormField(
                    controller: _passwordController,
                    decoration: const InputDecoration(
                      labelText: "Mot de passe",
                      hintText: "Votre Mot de passe",
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: true,
                    validator: (val) => val == null || val.isEmpty
                        ? "Ce champ est obligatoire"
                        : null,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  BlocConsumer<LoginBloc, LoginState>(
                    builder: (_, state) {
                      if (state is AuthenticationInProgress) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return ElevatedButton(
                        onPressed: () => _submitForm(context),
                        child: const Text("Connexion"),
                      );
                    },
                    listener: (_, state) {
                      if (state is Authenticated) {
                        Navigator.pushNamedAndRemoveUntil(
                          context,
                          kHomeRoute,
                          (_) => false,
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _submitForm(BuildContext context) async {
    if (_formKey.currentState?.validate() == true) {
      BlocProvider.of<LoginBloc>(context).add(LoginUser(
        username: _emailController.text,
        password: _passwordController.text,
      ));
    }
  }
}
