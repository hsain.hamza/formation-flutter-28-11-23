import 'package:flutter/material.dart';
import 'package:my_first_app/app/design_system/list_item.dart';

class ListScreen extends StatelessWidget {
  const ListScreen({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final  _usernameController = TextEditingController();
    final  _formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        title: Text("Ma liste"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Hello",
                      hintText: "Ici login",
                      suffixIcon: Icon(Icons.remove_red_eye),
                    ),
                    // keyboardType : TextInputType.visiblePassword,
                    // obscureText: true,
                    controller: _usernameController,
                    validator: (val){
                      if(val == null || val.isEmpty){
                        return "Une erreur ici !";
                      }
                      return null;
                    },
                  ),
                  ElevatedButton(
                    onPressed: () {
                      final valid = _formKey.currentState?.validate();

                      if(valid == true){
                        _usernameController.text = "FORCER MON TEXT";
                      }

                    },
                    child: Text("Login"),
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 200,
              color: Colors.red,
            ),
            SizedBox(
              height: 200,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  for (int i = 0; i < 25; i++)
                    Container(
                      width: 150,
                      height: 150,
                      color: Colors.blue,
                      margin: EdgeInsets.all(10),
                    ),
                ],
              ),
            ),
            for (int i = 0; i < 25; i++) ListItem(),
            Container(
              width: double.infinity,
              height: 200,
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}
