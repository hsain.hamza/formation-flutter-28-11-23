import 'package:flutter/material.dart';

class ListItem extends StatefulWidget {
  const ListItem({Key? key}) : super(key: key);

  @override
  State<ListItem> createState() { return _ListItemState(); }
}

class _ListItemState extends State<ListItem> {
  late int counter;

  @override
  void initState() {
    counter = 10;
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("ListItem > build");
    return SizedBox(
      width: double.infinity,
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: Container(
              width: 100,
              height: 100,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Mon premier titre",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text("Mon sous titre")
                ],
              ),
            ),
          ),
          SizedBox(
            width: 50,
            height: 100,
            child: IconButton(
              onPressed: () {
                setState(() {
                  counter ++;
                });

                print("ON TAP $counter");

              },
              icon: const Icon(
                Icons.star,
                color: Color(0xFFC62828),
              ),
            ),
          ),
          SizedBox(
            width: 50,
            height: 100,
            child: Center(child: Text("$counter")),
          ),
        ],
      ),
    );
  }
}
