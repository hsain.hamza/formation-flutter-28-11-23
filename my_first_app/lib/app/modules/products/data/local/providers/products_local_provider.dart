import 'package:my_first_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_first_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';

abstract class ProductsLocalProvider {
  Future init();

  Future store(List<ProductResponse> data);

  Future<List<ProductResponse>> fetch();

  Future clear();
}

class ProductsDbProvider extends ProductsLocalProvider {
  final Database _database = locator.get();
  final StoreRef _productsStore = intMapStoreFactory.store("Products_store");

  @override
  Future clear() async {
    await _productsStore.delete(_database);
  }

  @override
  Future<List<ProductResponse>> fetch() async {
    final dbProducts = await _productsStore.find(_database);

    return dbProducts
        .map((e) => ProductResponse.fromJson(e.value as Map<String, dynamic>))
        .toList();
  }

  @override
  Future init() async {}

  @override
  Future store(List<ProductResponse> data) async {
    await _database.transaction((transaction) async {
      await _productsStore.delete(transaction);
      for (ProductResponse prod in data) {
        await _productsStore.add(transaction, prod.toJson());
      }
    });
  }
}
