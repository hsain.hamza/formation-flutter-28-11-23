import 'package:my_first_app/app/modules/products/data/local/providers/products_local_provider.dart';
import 'package:my_first_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_first_app/app/modules/products/data/remote/providers/products_remote_provider.dart';
import 'package:my_first_app/core/di/locator.dart';

abstract class ProductsRepository {
  Future<List<ProductResponse>> retrieveAllProducts();
}

class ProductsRepositoryImpl extends ProductsRepository {

  final ProductsRemoteProvider _productsRemoteProvider = locator.get();
  final ProductsLocalProvider _productsLocalProvider = locator.get();


  @override
  Future<List<ProductResponse>> retrieveAllProducts() async {
    await _productsLocalProvider.init();
    try{
      final remoteProducts = await _productsRemoteProvider.retrieve();
      await _productsLocalProvider.store(remoteProducts);
      return remoteProducts;
    } catch(e){
      return _productsLocalProvider.fetch();
    }

  }

}