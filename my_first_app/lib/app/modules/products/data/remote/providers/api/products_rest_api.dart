import 'package:dio/dio.dart';
import 'package:my_first_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:retrofit/retrofit.dart';

part 'products_rest_api.g.dart';

@RestApi()
abstract class ProductsRestApi {

  factory ProductsRestApi(Dio dio, {String baseUrl}) = _ProductsRestApi;

  @GET("/api/products")
  Future<List<ProductResponse>> get();
}