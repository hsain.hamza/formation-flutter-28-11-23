import 'package:my_first_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_first_app/app/modules/products/data/remote/providers/api/products_rest_api.dart';
import 'package:my_first_app/core/http/http_dio_helper.dart';

abstract class ProductsRemoteProvider {
  Future<List<ProductResponse>> retrieve();
}

class ProductsRemoteProviderImpl extends ProductsRemoteProvider{
  final _productsRestApi = ProductsRestApi(HttpDioHelper.client);

  @override
  Future<List<ProductResponse>> retrieve() async {
    return _productsRestApi.get();
  }
}