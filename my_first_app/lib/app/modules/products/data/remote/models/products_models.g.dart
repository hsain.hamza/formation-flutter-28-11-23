// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductResponse _$ProductResponseFromJson(Map<String, dynamic> json) =>
    ProductResponse(
      id: json['product_id'] as String?,
      picture: json['picture'] as String?,
      title: json['title'] as String?,
      price: json['price'] as String?,
    );

Map<String, dynamic> _$ProductResponseToJson(ProductResponse instance) =>
    <String, dynamic>{
      'product_id': instance.id,
      'picture': instance.picture,
      'title': instance.title,
      'price': instance.price,
    };
