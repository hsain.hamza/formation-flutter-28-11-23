import 'package:json_annotation/json_annotation.dart';

part 'products_models.g.dart';

@JsonSerializable()
class ProductResponse {
  @JsonKey(name: "product_id")
  final String? id;
  final String? picture;
  final String? title;
  final String? price;

  ProductResponse({
    this.id,
    this.picture,
    this.title,
    this.price,
  });

  factory ProductResponse.fromJson(Map<String, dynamic> json) => _$ProductResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ProductResponseToJson(this);

}
