part of 'products_bloc.dart';

@immutable
abstract class ProductsState {}

class ProductsInitial extends ProductsState {}

class ProductsLoading extends ProductsState {}

class ProductsLoaded extends ProductsState {
  final List<ProductResponse> products;

  ProductsLoaded(this.products);
}

class ProductsError extends ProductsState {
  final dynamic error;

  ProductsError(this.error);
}
