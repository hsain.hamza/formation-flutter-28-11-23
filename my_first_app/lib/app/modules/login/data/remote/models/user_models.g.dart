// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginRequest _$LoginRequestFromJson(Map<String, dynamic> json) => LoginRequest(
      username: json['username'] as String?,
      password: json['password'] as String?,
    );

Map<String, dynamic> _$LoginRequestToJson(LoginRequest instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
    };

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      userInfos: json['user_infos'] == null
          ? null
          : UserInfosResponse.fromJson(
              json['user_infos'] as Map<String, dynamic>),
      token: json['token'] == null
          ? null
          : UserToken.fromJson(json['token'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'user_infos': instance.userInfos?.toJson(),
      'token': instance.token?.toJson(),
    };

UserInfosResponse _$UserInfosResponseFromJson(Map<String, dynamic> json) =>
    UserInfosResponse(
      id: json['id_collaborateur'] as String?,
      prenom: json['prenom'] as String?,
      nom: json['nom'] as String?,
      email: json['email'] as String?,
      poste: json['poste'] as String?,
    );

Map<String, dynamic> _$UserInfosResponseToJson(UserInfosResponse instance) =>
    <String, dynamic>{
      'id_collaborateur': instance.id,
      'prenom': instance.prenom,
      'nom': instance.nom,
      'email': instance.email,
      'poste': instance.poste,
    };

UserToken _$UserTokenFromJson(Map<String, dynamic> json) => UserToken(
      accessToken: json['access_token'] as String?,
      tokenType: json['token_type'] as String?,
      expiresIn: json['expiresIn'] as int?,
    );

Map<String, dynamic> _$UserTokenToJson(UserToken instance) => <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'expiresIn': instance.expiresIn,
    };
