import 'package:json_annotation/json_annotation.dart';

part 'user_models.g.dart';

@JsonSerializable()
class LoginRequest {
  final String? username;
  final String? password;

  LoginRequest({ this.username,  this.password});

  factory LoginRequest.fromJson(Map<String, dynamic> json) => _$LoginRequestFromJson(json);
  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LoginResponse {
  @JsonKey(name: "user_infos")
  final UserInfosResponse? userInfos;
  final UserToken? token;

  LoginResponse({
    this.userInfos,
    this.token,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);

}

@JsonSerializable(explicitToJson: true)
class UserInfosResponse {
  @JsonKey(name: "id_collaborateur")
  final String? id;
  final String? prenom;
  final String? nom;
  final String? email;
  final String? poste;

  UserInfosResponse({
    this.id,
    this.prenom,
    this.nom,
    this.email,
    this.poste,
  });

  factory UserInfosResponse.fromJson(Map<String, dynamic> json) => _$UserInfosResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UserInfosResponseToJson(this);

}

@JsonSerializable(explicitToJson: true)
class UserToken {
  @JsonKey(name: "access_token")
  final String? accessToken;
  @JsonKey(name: "token_type")
  final String? tokenType;
  final int? expiresIn;

  UserToken({
    this.accessToken,
    this.tokenType,
    this.expiresIn,
  });

  factory UserToken.fromJson(Map<String, dynamic> json) => _$UserTokenFromJson(json);
  Map<String, dynamic> toJson() => _$UserTokenToJson(this);

}
