import 'package:my_first_app/app/modules/login/data/remote/models/user_models.dart';
import 'package:my_first_app/app/modules/login/data/remote/providers/api/login_rest_api.dart';
import 'package:my_first_app/core/http/http_dio_helper.dart';

abstract class LoginRemoteProvider {
  Future<LoginResponse> login(String username, String password);
}


class LoginRemoteProviderImpl extends LoginRemoteProvider{
  final LoginRestApi _loginRestApi = LoginRestApi(HttpDioHelper.client);

  @override
  Future<LoginResponse> login(String username, String password){
    return _loginRestApi.login(LoginRequest(username: username, password: password));
  }
}