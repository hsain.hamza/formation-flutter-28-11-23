import 'package:dio/dio.dart';
import 'package:my_first_app/app/modules/login/data/remote/models/user_models.dart';
import 'package:retrofit/retrofit.dart';

part 'login_rest_api.g.dart';

@RestApi()
abstract class LoginRestApi {
  factory LoginRestApi(Dio dio, {String baseUrl}) = _LoginRestApi;

  @POST("/api/login")
  Future<LoginResponse> login( @Body() LoginRequest data );

}