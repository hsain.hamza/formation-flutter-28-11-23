import 'package:my_first_app/app/modules/login/data/remote/models/user_models.dart';
import 'package:my_first_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';

abstract class LoginLocalProvider{
  Future init();

  Future store(LoginResponse data);

  Future<LoginResponse> fetch();

  Future clear();

}

class LoginDbProvider extends LoginLocalProvider {

  final Database _database = locator.get();
  final StoreRef _loginStore = stringMapStoreFactory.store("login_store");

  @override
  Future clear() async {
    await _loginStore.record("user-data").delete(_database);
  }

  @override
  Future<LoginResponse> fetch() async {
    final dbData = await _loginStore.record("user-data").get(_database);
    if(dbData != null){
      return LoginResponse.fromJson(dbData as Map<String, dynamic>);
    }
    throw "USER_NOT_FOUND";
  }

  @override
  Future init() async {
  }

  @override
  Future store(LoginResponse data) async {
    await _loginStore.record("user-data").put(_database, data.toJson());
  }



}