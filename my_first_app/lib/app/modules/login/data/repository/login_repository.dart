import 'package:my_first_app/app/modules/login/data/local/providers/login_local_provider.dart';
import 'package:my_first_app/app/modules/login/data/remote/models/user_models.dart';
import 'package:my_first_app/app/modules/login/data/remote/providers/login_remote_provider.dart';
import 'package:my_first_app/core/di/locator.dart';

abstract class LoginRepository {
  Future<LoginResponse> getCurrentUser();
  Future<LoginResponse> loginUser(String username, String password);
  Future logoutUser();
}

class LoginRepositoryImpl extends LoginRepository {

  final LoginLocalProvider _loginLocalProvider = locator.get();
  final LoginRemoteProvider _loginRemoteProvider = locator.get();

  @override
  Future<LoginResponse> getCurrentUser() {
    return _loginLocalProvider.fetch();
  }

  @override
  Future<LoginResponse> loginUser(String username, String password) async {
    final userData =  await _loginRemoteProvider.login(username, password);
    await _loginLocalProvider.store(userData);
    return userData;
  }

  @override
  Future logoutUser() async {
    await _loginLocalProvider.clear();
  }

}