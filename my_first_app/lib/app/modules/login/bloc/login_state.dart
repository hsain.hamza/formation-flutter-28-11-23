part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class Authenticated extends LoginState {
  final LoginResponse user;
  Authenticated({required this.user});
}
class Unauthenticated extends LoginState {}

class AuthenticationInProgress extends LoginState {}


