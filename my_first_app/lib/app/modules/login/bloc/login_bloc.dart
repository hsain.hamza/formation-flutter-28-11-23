import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_first_app/app/modules/login/data/remote/models/user_models.dart';
import 'package:my_first_app/app/modules/login/data/repository/login_repository.dart';
import 'package:my_first_app/core/di/locator.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository _loginRepository = locator.get();

  LoginBloc() : super(Unauthenticated()) {
    on<LoginUser>((event, emit) async {
      emit(AuthenticationInProgress());
      try {
        final userData = await _loginRepository.loginUser(
          event.username,
          event.password,
        );
        emit(Authenticated(user: userData));
      } catch (e) {
        emit(Unauthenticated());
      }
    });

    on<CheckAuth>((event, emit) async {
      try{
        final loggedUser = await _loginRepository.getCurrentUser();
        emit(Authenticated(user: loggedUser));
      }catch(e){
        emit(Unauthenticated());
      }
    });

    on<LogoutUser>((event, emit) async {
      try{
        await _loginRepository.logoutUser();
      } catch(e){
        // nothing to do here :D
      }
      emit(Unauthenticated());
    });

  }
}
