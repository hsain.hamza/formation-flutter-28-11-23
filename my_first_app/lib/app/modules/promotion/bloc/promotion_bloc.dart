import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_first_app/app/modules/promotion/data/repository/promotion_repository.dart';
import 'package:my_first_app/core/di/locator.dart';

part 'promotion_event.dart';
part 'promotion_state.dart';

class PromotionBloc extends Bloc<PromotionEvent, PromotionState> {

  final PromotionRepository _promotionRepository = locator.get();

  PromotionBloc() : super(PromotionInitial()) {

    on<FetchPromotion>((event, emit) async {
      emit(PromotionLoading());
      try{
        final promo = await _promotionRepository.retrieveDetails();
        emit(PromotionLoaded(promo));
      } catch(e){
        emit(PromotionError(e));
      }
    });

  }

}
