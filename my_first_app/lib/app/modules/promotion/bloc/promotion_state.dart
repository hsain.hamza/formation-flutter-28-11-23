part of 'promotion_bloc.dart';

@immutable
abstract class PromotionState {}

class PromotionInitial extends PromotionState {}

class PromotionLoading extends PromotionState {}

class PromotionLoaded extends PromotionState {
  final PromotionResponse promotionResponse;

  PromotionLoaded(this.promotionResponse);
}

class PromotionError extends PromotionState {
  final dynamic error;

  PromotionError(this.error);
}
