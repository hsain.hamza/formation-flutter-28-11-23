import 'package:my_first_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/providers/api/promotion_rest_api.dart';
import 'package:my_first_app/core/http/http_dio_helper.dart';

abstract class PromotionRemoteProvider {
  Future<PromotionResponse> retrieve();
}

class PromotionRemoteProviderImpl extends PromotionRemoteProvider {
  final  _promotionRestApi = PromotionRestApi(HttpDioHelper.client);

  @override
  Future<PromotionResponse> retrieve() async {
    return _promotionRestApi.get();
  }
}


