import 'package:my_first_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
part 'promotion_rest_api.g.dart';

@RestApi()
abstract class PromotionRestApi {

  factory PromotionRestApi(Dio dio, {String baseUrl}) = _PromotionRestApi;

  @GET("/api/configuration")
  Future<PromotionResponse> get();
}