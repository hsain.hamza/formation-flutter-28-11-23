import 'package:my_first_app/app/modules/promotion/data/local/providers/promotion_local_provider.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/providers/promotion_remote_provider.dart';
import 'package:my_first_app/core/di/locator.dart';


abstract class PromotionRepository {
  Future<PromotionResponse> retrieveDetails();
}

class PromotionRepositoryImpl extends PromotionRepository {
  final PromotionRemoteProvider _promotionRemoteProvider = locator.get();
  final PromotionLocalProvider _promotionLocalProvider = locator.get();

  @override
  Future<PromotionResponse> retrieveDetails() async {
    await _promotionLocalProvider.init();
    try {
      final remoteData = await _promotionRemoteProvider.retrieve();
      // TODO : map data to entity
      await _promotionLocalProvider.store(remoteData);
      return remoteData;
    } catch (e) {
      return _promotionLocalProvider.fetch();
    }
  }
}
