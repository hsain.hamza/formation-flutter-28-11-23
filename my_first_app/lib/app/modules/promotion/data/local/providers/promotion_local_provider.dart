import 'package:my_first_app/app/modules/products/data/remote/models/products_models.dart';
import 'package:my_first_app/app/modules/promotion/data/remote/models/promotion_models.dart';
import 'package:my_first_app/core/di/locator.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PromotionLocalProvider{
  Future init();
  Future store(PromotionResponse data);
  Future<PromotionResponse> fetch();
  Future clear();
}

class PromotionDbProvider extends PromotionLocalProvider {

  final Database _database = locator.get();
  final StoreRef _promoStore = stringMapStoreFactory.store("promotion_store");

  /* final StoreRef _listStore = intMapStoreFactory.store("list_store");

  await _database.transaction((transaction) async {
  for(var i in [1,2,3,4]){
  await _listStore.add(transaction, i );
  }
  });
  final dataList = await _listStore.find(_database);
  dataList.map((e) => ProductResponse.fromJson(e.value as Map<String, dynamic>)).toList();
  */

  @override
  Future clear() async {
    await _promoStore.record("promo").delete(_database);
  }

  @override
  Future<PromotionResponse> fetch() async {
    final dbData = await _promoStore.record("promo").get(_database);
    if(dbData != null){
      return PromotionResponse.fromJson(dbData as Map<String, dynamic>);
    }
    throw "PROMO_NOT_FOUND";
  }

  @override
  Future init() async {}

  @override
  Future store(PromotionResponse data) async {
   await _promoStore.record("promo").put(_database, data.toJson());
  }

}

class PromotionLocalProviderImpl  extends PromotionLocalProvider {

  SharedPreferences? _dataSource;

  @override
  Future init() async {
    _dataSource ??= await SharedPreferences.getInstance();
  }

  @override
  Future<PromotionResponse> fetch() async {
    return PromotionResponse(
      promoMessage: _dataSource?.getString('PROMO_MESSAGE'),
      promoImage: _dataSource?.getString('PROMO_IMAGE'),
      promoSubtitle: _dataSource?.getString('PROMO_SUBTITLE'),
    );
  }

  @override
  Future<void> clear() async {
    await _dataSource?.remove('PROMO_MESSAGE');
    await _dataSource?.remove('PROMO_SUBTITLE');
    await _dataSource?.remove('PROMO_IMAGE');
  }

  @override
  Future store(PromotionResponse data) async {
    await _dataSource?.setString('PROMO_MESSAGE', data.promoMessage ?? '');
    await _dataSource?.setString('PROMO_SUBTITLE', data.promoSubtitle ?? '');
    await _dataSource?.setString('PROMO_IMAGE', data.promoImage ?? '');
  }

}